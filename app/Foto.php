<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    protected $fillable = ['imovel_id','url'];
    protected $guarded = ['id'];
    protected $table = 'fotos';
}
