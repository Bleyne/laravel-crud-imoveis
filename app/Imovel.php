<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Imovel extends Model
{
    use SoftDeletes;

    protected $fillable = ['titulo','tipo','cep','cidade','estado','bairro','numero', 'complemento','preco_venda',
        'preco_locacao','preco_temporada','area','dormitorio','suite','banheiro','sala','garagem'];
    protected $guarded = ['id', 'created_at', 'update_at', 'deleted_at'];
    protected $table = 'imoveis';

    public function fotos(){
        return $this->hasMany('App\Foto','imovel_id');
    }
}
