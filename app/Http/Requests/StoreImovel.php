<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreImovel extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo' => 'required|max:50',
            'tipo' => 'required|max:50',
            'cep' => 'required|max:10',
            'cidade' => 'required|max:50',
            'estado' => 'required|max:50',
            'bairro' => 'required|max:50',
            'numero' => 'required|max:50',
            'complemento' => 'max:50',
            'preco_venda' => 'max:20',
            'preco_locacao' => 'max:20',
            'preco_temporada' => 'max:20',
            'area' => 'max:8',
            'dormitorio' => 'max:8',
            'suite' => 'max:8',
            'banheiro' => 'max:8',
            'sala' => 'max:8',
            'garagem' => 'max:8',
            'fotos.*' => 'mimes:jpeg,jpg,png,gif|max:20000',
        ];

    }
}
