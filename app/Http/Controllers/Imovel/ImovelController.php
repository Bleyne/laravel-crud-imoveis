<?php

namespace App\Http\Controllers\Imovel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Imovel;
use App\Foto;
use App\Http\Requests\StoreImovel;

class ImovelController extends Controller
{

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ( $request->busca ) {
            $imoveis = Imovel::where('titulo', '=', $request->busca)->paginate(15);
        } else {
            $imoveis = Imovel::paginate(15);
        }

        return view('imoveis.index', compact('imoveis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('imoveis.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreImovel $request)
    {
        $imovel = new Imovel;
        $imovel->titulo = $request->titulo;
        $imovel->tipo = $request->tipo;
        $imovel->cep = $request->cep;
        $imovel->cidade = $request->cidade;
        $imovel->estado = $request->estado;
        $imovel->bairro = $request->bairro;
        $imovel->numero = $request->numero;
        $imovel->complemento = $request->complemento;
        $imovel->preco_venda = $request->preco_venda;
        $imovel->preco_locacao = $request->preco_locacao;
        $imovel->preco_temporada = $request->preco_temporada;
        $imovel->area = $request->area;
        $imovel->dormitorio = $request->dormitorio;
        $imovel->suite = $request->suite;
        $imovel->banheiro = $request->banheiro;
        $imovel->sala = $request->sala;
        $imovel->garagem = $request->garagem;
        $imovel->save();

        if ($request->file('fotos')) {
            foreach ($request->file('fotos') as $fotoRow) {
                $nomeArquivo = uniqid() . '.' . $fotoRow->getClientOriginalExtension();
                $fotoRow->move('fotos',$nomeArquivo);
                $url = '//'.$_SERVER['SERVER_NAME'] .'/fotos/'.$nomeArquivo;

                $foto = new Foto;
                $foto->url = $url;
                $imovel->fotos()->save($foto);
            }
        }

        return redirect()->route('imovel.index')->with('message', 'Imóvel cadastrado com Sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $imovel = Imovel::findOrFail($id);
        return view('imoveis.edit', compact('imovel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreImovel $request, $id)
    {
        $imovel = Imovel::findOrFail($id);
        $imovel->titulo = $request->titulo;
        $imovel->tipo = $request->tipo;
        $imovel->cep = $request->cep;
        $imovel->cidade = $request->cidade;
        $imovel->estado = $request->estado;
        $imovel->bairro = $request->bairro;
        $imovel->numero = $request->numero;
        $imovel->complemento = $request->complemento;
        $imovel->preco_venda = $request->preco_venda;
        $imovel->preco_locacao = $request->preco_locacao;
        $imovel->preco_temporada = $request->preco_temporada;
        $imovel->area = $request->area;
        $imovel->dormitorio = $request->dormitorio;
        $imovel->suite = $request->suite;
        $imovel->banheiro = $request->banheiro;
        $imovel->sala = $request->sala;
        $imovel->garagem = $request->garagem;
        $imovel->save();

        if ($request->file('fotos')) {
            foreach ($request->file('fotos') as $fotoRow) {
                $nomeArquivo = uniqid() . '.' . $fotoRow->getClientOriginalExtension();
                $fotoRow->move('fotos',$nomeArquivo);
                $url = '//'.$_SERVER['SERVER_NAME'] .'/fotos/'.$nomeArquivo;

                $foto = new Foto;
                $foto->url = $url;

                $imovel->fotos()->save($foto);
            }
        }

        if ($request->jsonFotos) {
            $jsonFotos = explode(',',$request->jsonFotos);
            array_pop($jsonFotos);
            foreach ($jsonFotos as $fotoId) {
                $fotoDelete = Foto::findOrFail($fotoId);
                $fotoDelete->delete();
            }
        }


        return redirect()->route('imovel.index')->with('message', 'Imóvel atualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $imovel = Imovel::findOrFail($id);
        $imovel->delete();
        return redirect()->route('imovel.index')->with('message', 'Imóvel removido!');
    }

    /**
     * Importa arquivos XML
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        if ($request->file('xml-file')) {
            $importFile = $request->file('xml-file');
            $xmlString = file_get_contents( $importFile->getPathname());
            $xml = simplexml_load_string($xmlString, "SimpleXMLElement", LIBXML_NOCDATA);

            foreach ($xml->Imoveis->Imovel as $row) {
                $imovel = new Imovel;
                $imovel->titulo = reset($row->CodigoImovel);
                $imovel->tipo = reset($row->TipoImovel);
                $imovel->cep = reset($row->CEP);
                $imovel->cidade = reset($row->Cidade);
                $imovel->estado = reset($row->UF);
                $imovel->bairro = reset($row->Bairro);
                $imovel->numero = reset($row->Numero);
                $imovel->complemento = reset($row->Complemento);

                /** Formatar Valor para padrão decimal 15,2*/
                $formatarPreco = function ($valor) {
                    $valor = reset($valor);
                    $valor = str_replace('.', '', $valor);
                    return number_format((float)$valor, 2, '.', '');
                };

                $imovel->preco_venda = $formatarPreco($row->PrecoVenda);
                $imovel->preco_locacao = $formatarPreco($row->PrecoLocacao);
                $imovel->preco_temporada = $formatarPreco($row->PrecoLocacaoTemporada);
                $imovel->area = reset($row->AreaUtil);
                $imovel->dormitorio = reset($row->QtdDormitorios);
                $imovel->suite = reset($row->QtdSuites);
                $imovel->banheiro = reset($row->QtdBanheiros);
                $imovel->sala = reset($row->QtdSalas);
                $imovel->garagem = reset($row->QtdVagas);
                $imovel->save();

                foreach ($row->Fotos->Foto as $fotoImovel) {
                    $foto = new Foto();
                    $foto->url = (reset($fotoImovel->URLArquivo));
                    $imovel->fotos()->save($foto);
                }

            }
            return redirect()->route('imovel.index')->with('message', 'Arquivo Importardo!');
        }


        return view('imoveis.import');
    }
}
