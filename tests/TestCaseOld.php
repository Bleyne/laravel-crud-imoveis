<?php

namespace Tests;

use Laravel\BrowserKitTesting\TestCase as BaseTestCase;

abstract class TestCaseOld extends BaseTestCase
{
    use CreatesApplication;

    public $baseUrl = 'http://imovel.local';
}
