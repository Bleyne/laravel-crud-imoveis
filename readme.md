# Laravel Crud Imóveis

## Introdução

Criar uma área administrativa podendo utilizar um template admin
Implementar área de login, pesquisa por imóvel, cadastro, edição e exclusão do imóvel, importar imóvel via XML
 
Telas:
1. Tela de login
e-mail e senha
 
2. Tela de listagem dos imóveis com campo para busca
busca por id/código do imóvel
paginação para listagem do imóvel
 
3. Tela de cadastro/edição/exclusão do imóvel
Deve conter;
 
Título do imóvel
Código do imóvel
Tipo do imóvel (casa, apartamento)
 
Endereço do imóvel
Consumir uma API online que a partir do fornecimento do cep retorne as informações logradouro
Campos - CEP, Cidade, Estado, Bairro, Número, Complemento
 
Preço do imóvel
Informar valor do imóvel para Venda, Locação ou Temporada
 
Área do imóvel
metro quadrado
 
Quantidade de dormitórios, suítes, banheiros, salas e garagem
 
Descrição do imóvel
 
Imagem do imóvel
Upload de imagem do imóvel
 
4. Tela para importar imóveis consumidos via XML - http://imob21.com.br/acc/imob21/publish/integracao.xml
Importar somente as informações disponíveis no cadastro do imóvel
 
5. Versionamento
Versionar o projeto no github e enviar a url para avaliação.

## Instação

Após fazer download do projeto, utilizar composer para instalar dependências

```bash
$ composer install
```

Adicionar um registro ao vhost, modificar Directory e DocumentRoot para diretorio correspondente ao Apache

```bash
<VirtualHost *:80>
    ServerName imoveis.local
     DocumentRoot C:/wamp64/www/laravel-crud-imoveis/public
     <Directory C:/wamp64/www/laravel-crud-imoveis/public>
         DirectoryIndex index.php
         AllowOverride All
         Order allow,deny
         Allow from all
     </Directory>
</VirtualHost>
```

Editar arquivo hosts do Sistema

```bash
Abra a pasta %WinDir%\System32\Drivers\Etc (caso seja sistema Windows)
Abrir arquivo hosts
Adicionar a seguinte linha ao arquivo e salvar
127.0.0.1 imoveis.local
```
%WinDir%\System32\Drivers\Etc

Modificar variáveis de ambiente para upload de arquivos

```bash
file_uploads = On
post_max_size = 50M
upload_max_filesize = 50M
session.upload_progress.enabled = On
session.upload_progress.freq =  "1%"
session.upload_progress.min_freq = "1"
```

Executar Migration

```bash
php artisan migrate 
```

Criar arquivo .env com dados de acesso ao banco