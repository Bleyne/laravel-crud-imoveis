<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImoveisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imoveis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 50);
            $table->string('tipo', 50)->nullable();
            $table->string('cep', 10);
            $table->string('cidade', 50);
            $table->string('estado', 50);
            $table->string('bairro', 50);
            $table->string('numero', 50);
            $table->string('complemento', 50)->nullable();
            $table->decimal('preco_venda',15,2)->nullable();
            $table->decimal('preco_locacao',15,2)->nullable();
            $table->decimal('preco_temporada',15,2)->nullable();
            $table->unsignedBigInteger('area')->nullable();
            $table->unsignedSmallInteger('dormitorio')->nullable();
            $table->unsignedSmallInteger('suite')->nullable();
            $table->unsignedSmallInteger('banheiro')->nullable();
            $table->unsignedSmallInteger('sala')->nullable();
            $table->unsignedSmallInteger('garagem')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('imoveis');
    }
}
