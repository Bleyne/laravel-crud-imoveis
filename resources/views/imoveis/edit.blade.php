@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Adicionar Imóvel</h1>

        <form method="post" enctype="multipart/form-data" id="imovel-form" action="/imovel/{{$imovel->id}}">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="jsonFotos" id="jsonFotos">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('titulo') ? ' has-error' : '' }}">
                        <label for="titulo">Título (Código Imóvel)</label>
                        <input type="text" name="titulo" placeholder="Título (Código Imóvel)" class="form-control"
                               id="titulo" value="{{ $imovel->titulo }}">
                        @if ($errors->has('titulo'))
                            <span class="help-block"><strong>{{ $errors->first('titulo') }}</strong></span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('tipo') ? ' has-error' : '' }}">
                        <label for="tipo">Tipo</label>
                        <select name="tipo" class="form-control" id="tipo">
                            <option value="Casa" {{ 'Casa' == $imovel->tipo ? 'selected' : ''}}>Casa</option>
                            <option value="Apartamento" {{ 'Apartamento' == $imovel->tipo ? 'selected' : ''}}>Apartamento</option>
                        </select>
                        @if ($errors->has('tipo'))
                            <span class="help-block"><strong>{{ $errors->first('tipo') }}</strong></span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('cep') ? ' has-error' : '' }}">
                        <label for="cep">CEP</label>
                        <input type="text" name="cep" placeholder="CEP" class="form-control" id="cep" value="{{ $imovel->cep }}"
                               maxlength="9">
                        @if ($errors->has('cep'))
                            <span class="help-block"><strong>{{ $errors->first('cep') }}</strong></span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('cidade') ? ' has-error' : '' }}">
                        <label for="cidade">Cidade</label>
                        <input type="text" name="cidade" placeholder="Cidade" class="form-control" id="cidade" value="{{ $imovel->cidade }}">
                        @if ($errors->has('cidade'))
                            <span class="help-block"><strong>{{ $errors->first('cidade') }}</strong></span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('estado') ? ' has-error' : '' }}">
                        <label for="estado">Estado</label>
                        <input type="text" name="estado" placeholder="Estado" class="form-control" id="estado" value="{{ $imovel->estado }}">
                        @if ($errors->has('estado'))
                            <span class="help-block"><strong>{{ $errors->first('estado') }}</strong></span>
                        @endif
                    </div>


                    <div class="form-group{{ $errors->has('numero') ? ' has-error' : '' }}">
                        <label for="numero">Numero</label>
                        <input type="text" name="numero" placeholder="Numero" class="form-control" id="numero" value="{{ $imovel->numero }}">
                        @if ($errors->has('numero'))
                            <span class="help-block"><strong>{{ $errors->first('numero') }}</strong></span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('complemento') ? ' has-error' : '' }}">
                        <label for="complemento">Complemento</label>
                        <input type="text" name="complemento" placeholder="Complemento" class="form-control"
                               id="complemento" value="{{ $imovel->complemento }}">
                        @if ($errors->has('complemento'))
                            <span class="help-block"><strong>{{ $errors->first('complemento') }}</strong></span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('bairro') ? ' has-error' : '' }}">
                        <label for="bairro">Bairro</label>
                        <input type="text" name="bairro" placeholder="Bairro" class="form-control" id="bairro" value="{{ $imovel->bairro }}">
                        @if ($errors->has('bairro'))
                            <span class="help-block"><strong>{{ $errors->first('bairro') }}</strong></span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('preco_venda') ? ' has-error' : '' }}">
                        <label for="preco_venda">Preco Venda</label>
                        <input type="text" name="preco_venda" placeholder="Preço Venda" class="form-control" value="{{ $imovel->preco_venda }}">
                        @if ($errors->has('preco_venda'))
                            <span class="help-block"><strong>{{ $errors->first('preco_venda') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('preco_locacao') ? ' has-error' : '' }}">
                        <label for="preco_locacao">Preco Locação</label>
                        <input type="text" name="preco_locacao" placeholder="Preco Locação" class="form-control"
                               value="{{ $imovel->preco_locacao }}">
                        @if ($errors->has('preco_locacao'))
                            <span class="help-block"><strong>{{ $errors->first('preco_locacao') }}</strong></span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('preco_temporada') ? ' has-error' : '' }}">
                        <label for="preco_temporada">Preco Temporada</label>
                        <input type="text" name="preco_temporada" placeholder="Preco Temporada" class="form-control"
                               value="{{ $imovel->preco_temporada }}">
                        @if ($errors->has('preco_temporada'))
                            <span class="help-block"><strong>{{ $errors->first('preco_temporada') }}</strong></span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('area') ? ' has-error' : '' }}">
                        <label for="area">Área (m²)</label>
                        <input type="text" name="area" placeholder="Área (m²)" class="form-control" value="{{ $imovel->area }}">
                        @if ($errors->has('area'))
                            <span class="help-block"><strong>{{ $errors->first('area') }}</strong></span>
                        @endif
                    </div>


                    <div class="form-group{{ $errors->has('dormitorio') ? ' has-error' : '' }}">
                        <label for="dormitorio">Qtd Dormitorios</label>
                        <input type="text" name="dormitorio" placeholder="Qtd Dormitorios" class="form-control"
                               value="{{ $imovel->dormitorio }}">
                        @if ($errors->has('dormitorio'))
                            <span class="help-block"><strong>{{ $errors->first('dormitorio') }}</strong></span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('suite') ? ' has-error' : '' }}">
                        <label for="suite">Qtd Suites</label>
                        <input type="text" name="suite" placeholder="Qtd Suites" class="form-control" value="{{ $imovel->suite }}">
                        @if ($errors->has('suite'))
                            <span class="help-block"><strong>{{ $errors->first('suite') }}</strong></span>
                        @endif
                    </div>


                    <div class="form-group{{ $errors->has('banheiro') ? ' has-error' : '' }}">
                        <label for="banheiro">Qtd Banheiros</label>
                        <input type="text" name="banheiro" placeholder="Qtd Banheiros" class="form-control" value="{{ $imovel->banheiro }}">
                        @if ($errors->has('banheiro'))
                            <span class="help-block"><strong>{{ $errors->first('banheiro') }}</strong></span>
                        @endif
                    </div>


                    <div class="form-group{{ $errors->has('sala') ? ' has-error' : '' }}">
                        <label for="sala">Qtd Salas</label>
                        <input type="text" name="sala" placeholder="Qtd Salas" class="form-control" value="{{ $imovel->sala }}">
                        @if ($errors->has('sala'))
                            <span class="help-block"><strong>{{ $errors->first('sala') }}</strong></span>
                        @endif
                    </div>


                    <div class="form-group{{ $errors->has('garagem') ? ' has-error' : '' }}">
                        <label for="garagem">Qtd Garagens</label>
                        <input type="text" name="garagem" placeholder="Qtd Garagens" class="form-control" value="{{ $imovel->garagem }}">
                        @if ($errors->has('garagem'))
                            <span class="help-block"><strong>{{ $errors->first('garagem') }}</strong></span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('fotos.*') ? ' has-error' : '' }}">
                        <label for="fotos">Fotos</label>
                        <input type="file" name="fotos[]" multiple="">
                        @if ($errors->has('fotos.*'))
                            <span class="help-block"><strong>{{ $errors->first('fotos.*') }}</strong></span>
                        @endif
                    </div>
                    <input type="submit" name="submit" class="btn btn-primary" value="Enviar">
                </div>
            </div>
            <div class="row">
                <?php
                foreach ($imovel->fotos as $foto) {
                    echo '<div class="col-md-2" id="foto-'.$foto->id.'">';
                    echo '<img width="200" src="'.$foto->url.'">';
                    echo '<button type="button" class="btn btn-danger btn-xs remover-foto" foto-id="'.$foto->id.'"><span class="glyphicon glyphicon-remove"></span></button>';
                    echo '</div>';
                }
                ?>
            </div>
        </form>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ URL::asset('js/imoveis.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.mask.js') }}"></script>
@endsection
