@extends('layouts.app')

@section('content')

    @if (\Session::has('message'))
        <div class="alert alert-success">
            <ul>
                <li>{!! \Session::get('message') !!}</li>
            </ul>
        </div>
    @endif

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>Imóveis</h1>
            </div>
            <form method="get" id="busca-form" action="/imovel">

                <div class="col-md-6">
                    <div id="custom-search-input" style="margin-top: 15px;">
                        <div class="input-group col-md-12">
                            <input type="text" name="busca" placeholder="Código Imóvel" class="form-control input-md"
                                   value=""> <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Título(Cod. Imóvel)</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">CEP</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Cidade</th>
                        <th scope="col">Ações</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($imoveis as $imovel)

                        <tr>
                            <th scope="row">{{ $imovel->id }}</th>
                            <td>{{ $imovel->titulo }}</td>
                            <td>{{ $imovel->tipo }}</td>
                            <td>{{ $imovel->cep }}</td>
                            <td>{{ $imovel->estado }}</td>
                            <td>{{ $imovel->cidade }}</td>
                            <td>
                                <a href="/imovel/{{ $imovel->id }}/edit" class="btn btn-xs"><span
                                            class="glyphicon glyphicon-pencil"></span></a>
                                <form method="post" id="imovel-form" action="/imovel/{{$imovel->id}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button type="submit" name="submit" class="btn btn-danger btn-xs" value="Enviar">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </button>
                                </form>
                            </td>
                        </tr>

                    @endforeach

                    </tbody>
                </table>

                {{ $imoveis->links() }}
            </div>
        </div>
    </div>
@endsection