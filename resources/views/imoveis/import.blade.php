@extends('layouts.app')

@section('content')
    <div class="container">
        <form method="POST" name="upload-form" enctype="multipart/form-data" id="upload-form">
            {{ csrf_field() }}
            <div class="form-element">
                <label for="xml-file">XML Upload</label> <input type="file" name="xml-file" id="xml-file">
            </div>
            <br>
            <button class="btn btn-primary">Importar</button>
        </form>
    </div>
@endsection