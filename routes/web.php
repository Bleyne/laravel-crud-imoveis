<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('imovel\index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/import', 'Imovel\ImovelController@import')->name('import');
Route::get('/import', 'Imovel\ImovelController@import')->name('import');

Route::resource('imovel', 'Imovel\ImovelController');